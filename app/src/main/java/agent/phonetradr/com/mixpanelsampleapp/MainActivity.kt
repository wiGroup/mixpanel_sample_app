package agent.phonetradr.com.mixpanelsampleapp

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem

import kotlinx.android.synthetic.main.activity_main.*
import com.mixpanel.android.mpmetrics.MixpanelAPI
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val mixpanel = MixpanelAPI.getInstance(this.applicationContext, "6c551ba18e6b56deeb5f71885184c4b9")
        mixpanel.identify("steeve_test_in_app_message")
        mixpanel.alias("test_alias", "steeve_test_in_app_message")
        mixpanel.people.set("\$email", "steeve.fong@hotmail.com")
        mixpanel.people.identify("steeve_test_in_app_message")
        mixpanel.people.set("name", "Steeeeeeeve")

        mixpanel.flush()

        fab.setOnClickListener { view ->
            mixpanel.track("click test event")
            mixpanel.people.showNotificationIfAvailable(this);
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
